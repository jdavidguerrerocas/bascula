#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3F,16,2);  // sino funciona cambiar la direccion a 0x3F
//conexion
// SDA= pin A4
// SCL= pin A5
// RX MOD RS232 - TX ARDUINO NANO
// TX MOD RS232 - RX ARDUINO NANO 
// VCC MOD - 3.3V
int nBytes = 18;
int Byt;
double peso = 0;
int inByte[18];
boolean datosSWA[8];
boolean datosSWB[8];
boolean datosSWC[8];
int pesob[6];

void setup()
{
  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  Serial.begin(9600);
  lcd.write("Inicio Comunicacion");
  delay(1000);
  lcd.clear();
}

void loop() {
  if (Serial.available() > 0) {
    lcd.print("Recibiendo datos...");
    //comprueba el encabezado 02
    inByte[0] = Serial.read();
    if (inByte[0] == 2) {

      for (int i = 1; i < nBytes; i++) {
        while (!Serial.available()); //recibe todo el formato de comunicacion;
        inByte[i] = Serial.read();
      }
      peso=0;
      getPeso();
      getWords();

      if(datosSWA[6] == 0 && datosSWA[5] == 1 && datosSWA[2] == 0 && datosSWA[1] == 0 && datosSWA[0] == 0){
        peso = peso*100;
        
      }
      if(datosSWA[2] == 0 && datosSWA[1] == 0 && datosSWA[0] == 1){
        peso = peso*10;
        
      }
      if(datosSWA[2] == 0 && datosSWA[1] == 1 && datosSWA[0] == 0){
        peso = peso*1;
         
      }
      if(datosSWA[2] == 0 && datosSWA[1] == 1 && datosSWA[0] == 1){
        peso = peso/10;
        
      }
      if(datosSWA[2] == 1 && datosSWA[1] == 0 && datosSWA[0] == 0){
        peso = peso/100;
         
      }
       if(datosSWA[2] == 1 && datosSWA[1] == 0 && datosSWA[0] == 1){
        
        peso = peso/1000;
      }
       if(datosSWA[2] == 1 && datosSWA[1] == 1 && datosSWA[0] == 0){
        peso = peso/10000;
        
      }
       if(datosSWA[2] == 1 && datosSWA[1] == 1 && datosSWA[0] == 1){
        peso = peso/100000;
        
      }

//Consideraciones Palabra clave B

    if(datosSWB[1]==1){
      peso = peso*-1;
      
    }
     
    if(datosSWB[0]==0){
      
      Serial.print(peso);
      lcd.setCursor(0, 1);
      lcd.print(peso);
    }
     if( datosSWB[4]==0){
      Serial.print("KG");
      lcd.setCursor(0, 0);
      lcd.print("PESO (KG):");
    }
    if( datosSWB[4]==1){
      //lcd.setCursor(0, 0);
      //lcd.print("PESO (LB):");
    }
   

    }
  }

  }

void getPeso() {

     int multiplier = 1;
     pesob[5] = inByte[4] & 0b00001111;
     pesob[4] = inByte[5] & 0b00001111;
     pesob[3] = inByte[6] & 0b00001111;
     pesob[2] = inByte[7] & 0b00001111;
     pesob[1] = inByte[8] & 0b00001111;
     pesob[0] = inByte[9] & 0b00001111;

    
    for (int i = 0; i < 7; i++ )
    {
        peso += (multiplier * pesob[i]);
        multiplier *= 10;
    }
     
 }

  
 
void getWords (){
  Byt = inByte[1];
  char mask =1;
  for (int i = 0; i < 8; i++) {
    datosSWA[i] = (Byt & (mask << i)) !=0;
  }
  Byt = inByte[2];
  for (int i = 0; i < 8; i++) {
    datosSWB[i] = (Byt & (mask << i)) !=0;
  }
  Byt = inByte[3];
  for (int i = 0; i < 8; i++) {
    datosSWC[i] = (Byt & (mask << i)) !=0;
  }
  
}



